$(document).ready(function() {

        // Transition effect for navbar and back-to-top icon
        $(window).scroll(function() {
          // checks if window is scrolled more than 500px, adds/removes solid class
          if($(this).scrollTop() > 550) { 
              $('.navbar').addClass('solid');
              $('.back-to-top').addClass('visible'); 
          } else {
              $('.navbar').removeClass('solid');
              $('.back-to-top').removeClass('visible');  
          }

        });


        // Scrolling effect for Arrow icons
        $("#scrollIcon").click(function(e) {
            e.preventDefault();
            $.scrollTo($("#about"), 1000);
        });
        $("#nav-about").click(function(e) {
            e.preventDefault();
            $.scrollTo($("#about"), 1000);
        });
        $("#nav-portfolio").click(function(e) {
            e.preventDefault();
            $.scrollTo($("#portfolio"), 1000);
        });
        $("#nav-contact").click(function(e) {
            e.preventDefault();
            $.scrollTo($("#contact"), 1000);
        });
        $(".navbar-brand").click(function(e) {
            e.preventDefault();
            $.scrollTo(0, 1000);
        });
          
      });






linePage();
cycleText();

function linePage() {
  var splitMe = $(".sentence");

  splitMe.each(function(index) {
    var text = $(this).html();
    var output = "";

    //split all letters into spans
    for (var i = 0, len = text.length; i < len; i++) {
      output += '<span data-index="' + i + '">' + text[i] + "</span>";
    }

    //put it in the html
    $(this).html(output);

    //check the offset of each letter to figure out where the line breaks
    var prev = 0;
    var parts = [];
    $(this).find("span").each(function(i) {
      if ($(this).offset().top > prev) {
        parts.push(i);
        prev = $(this).offset().top;
      }
    });

    parts.push(text.length);

    //create final
    var finalOutput = "";

    parts.forEach(function(endPoint, i) {
      if (endPoint > 0) {
        finalOutput +=
          '<span data-line="' +
          i +
          '" class="line-wrap"><span class="line-inner">' +
          text.substring(parts[i - 1], parts[i]) +
          "</span></span>";
      }
    });

    $(this).html(finalOutput);
    $(this).addClass("lined");
  });
}

function cycleText() {
  setInterval(function() {
    $(".sentence").toggleClass("sentence--show");
  }, 4000);

  setTimeout(function() {
    $(".sentence").toggleClass("sentence--show");
  }, 1000);
}
